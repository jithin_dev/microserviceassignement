package com.jithin.productService.controller;

import com.jithin.productService.dto.request.ProductRequestDTO;
import com.jithin.productService.dto.response.ProductResponseDTO;
import com.jithin.productService.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

@RestController
public class ProductController {

    private final ProductService productService;

    @Inject
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/product")
    ResponseEntity<List<ProductResponseDTO>> getAllProducts() {
        List<ProductResponseDTO> allProducts = productService.getAllProducts();
        return new ResponseEntity<>(allProducts, HttpStatus.OK);
    }

    @GetMapping("/product/{id}")
    ResponseEntity<ProductResponseDTO> getProductById(@PathVariable("id") int id) {
        ProductResponseDTO product = productService.getProduct(id);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }

    @PostMapping("/product")
    ResponseEntity<ProductResponseDTO> createProduct(@RequestBody ProductRequestDTO dto) {
        ProductResponseDTO productCreated = productService.submitProduct(dto);
        return new ResponseEntity<>(productCreated, HttpStatus.CREATED);
    }

    @PutMapping("/product/{id}")
    ResponseEntity<ProductResponseDTO> updateProduct(@PathVariable("id") int id, @RequestBody ProductRequestDTO dto) {
        ProductResponseDTO productCreated = productService.updateProduct(id, dto);
        return new ResponseEntity<>(productCreated, HttpStatus.OK);
    }

    @DeleteMapping("/product/{id}")
    ResponseEntity<ProductResponseDTO> deleteProduct(@PathVariable("id") int id) {
        productService.deleteProduct(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
