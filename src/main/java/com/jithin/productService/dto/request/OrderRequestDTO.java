package com.jithin.productService.dto.request;

import java.util.List;

public class OrderRequestDTO {

    private List<Integer> products;

    public List<Integer> getProducts() {
        return products;
    }

    public void setProducts(List<Integer> products) {
        this.products = products;
    }
}
