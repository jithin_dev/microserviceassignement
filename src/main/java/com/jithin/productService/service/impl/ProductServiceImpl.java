package com.jithin.productService.service.impl;

import com.jithin.productService.dto.request.ProductRequestDTO;
import com.jithin.productService.dto.response.ProductResponseDTO;
import com.jithin.productService.exception.ProductNotFoundException;
import com.jithin.productService.model.Product;
import com.jithin.productService.repository.ProductRepository;
import com.jithin.productService.service.ProductService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Inject
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<ProductResponseDTO> getAllProducts() {
        List<Product> products = productRepository.findAll();
        return products.stream().map(ProductServiceImpl::makeProductResponseDTO).collect(Collectors.toList());
    }

    @Override
    public ProductResponseDTO getProduct(int id) {
        Product product = productRepository.findById(id).orElse(null);
        if (product == null) {
            throw new ProductNotFoundException("The product with id: " + id + " not found");
        }
        return makeProductResponseDTO(product);
    }

    @Override
    public ProductResponseDTO submitProduct(ProductRequestDTO dto) {
        Product product = new Product();
        product.setName(dto.getName());
        product.setPrice(dto.getPrice());
        product.setDeleted(dto.isDeleted());
        productRepository.save(product);
        return makeProductResponseDTO(product);
    }

    @Override
    public ProductResponseDTO updateProduct(int id, ProductRequestDTO dto) {
        Product product = productRepository.findById(id).orElse(null);
        if (product == null) {
            throw new ProductNotFoundException("The product with id: " + id + " not found");
        }

        product.setName(dto.getName());
        product.setPrice(dto.getPrice());
        product.setDeleted(dto.isDeleted());
        productRepository.save(product);
        return makeProductResponseDTO(product);
    }

    @Override
    public void deleteProduct(int id) {
        Product productFromDB = productRepository.findById(id).orElse(null);
        if (productFromDB == null) {
            throw new ProductNotFoundException("The product with id: " + id + " not found");
        }
        //not deleting from table since orders are mapped with products
        productFromDB.setDeleted(true);
    }

    public static ProductResponseDTO makeProductResponseDTO(Product product) {
        ProductResponseDTO dto = new ProductResponseDTO();
        dto.setProductId(product.getId());
        dto.setProductName(product.getName());
        dto.setPrice(product.getPrice());
        dto.setDeleted(product.isDeleted());
        return dto;
    }
}
