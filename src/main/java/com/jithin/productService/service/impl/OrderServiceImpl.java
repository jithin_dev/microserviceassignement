package com.jithin.productService.service.impl;

import com.jithin.productService.dto.request.OrderRequestDTO;
import com.jithin.productService.dto.response.OrderResponseDTO;
import com.jithin.productService.dto.response.ProductResponseDTO;
import com.jithin.productService.exception.OrderNotFoundException;
import com.jithin.productService.model.Order;
import com.jithin.productService.model.Product;
import com.jithin.productService.repository.OrderRepository;
import com.jithin.productService.repository.ProductRepository;
import com.jithin.productService.service.OrderService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;

    @Inject
    public OrderServiceImpl(
            OrderRepository orderRepository
            , ProductRepository productRepository
    ) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    @Override
    public OrderResponseDTO createOrder(OrderRequestDTO dto) {
        Set<Product> products = dto.getProducts().stream()
                .map(pid -> productRepository.findById(pid).orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());

        double totalPrice = products.stream().mapToDouble(Product::getPrice).sum();
        Order order = new Order();
        Date now = new Date();
        order.setOrderDate(now);
        order.setProducts(products);
        order.setTotalPrice(totalPrice);
        orderRepository.save(order);
        return makeOrderResponseDTO(order);
    }

    @Override
    public OrderResponseDTO updateOrder(int id, OrderRequestDTO dto) {
        Order order = orderRepository.findById(id).orElse(null);
        if (order == null) {
            throw new OrderNotFoundException("The order with id: " + id + " not found");
        }

        Set<Product> products = dto.getProducts().stream()
                .map(pid -> productRepository.findById(pid).orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());

        double totalPrice = products.stream().mapToDouble(Product::getPrice).sum();

        Date now = new Date();
        order.setOrderDate(now);
        order.setProducts(products);
        order.setTotalPrice(totalPrice);
        orderRepository.save(order);
        return makeOrderResponseDTO(order);
    }

    @Override
    public List<OrderResponseDTO> getOrders() {
        List<Order> orders = orderRepository.findAll();
        return orders.stream().map(this::makeOrderResponseDTO).collect(Collectors.toList());
    }

    @Override
    public OrderResponseDTO getOrder(int id) {
        Order order = orderRepository.findById(id).orElse(null);
        if (order == null) {
            throw new OrderNotFoundException("The order with id: " + id + " not found");
        }

        return makeOrderResponseDTO(order);
    }

    @Override
    public void deleteOrder(int id) {
        Order order = orderRepository.findById(id).orElse(null);
        if (order == null) {
            throw new OrderNotFoundException("The order with id: " + id + " not found");
        }

        orderRepository.delete(order);
    }

    public OrderResponseDTO makeOrderResponseDTO(Order order) {
        OrderResponseDTO dto = new OrderResponseDTO();
        dto.setOderId(order.getId());
        dto.setOrderDate(order.getOrderDate());
        dto.setTotalPrice(order.getTotalPrice());
        Set<Product> products = order.getProducts();
        List<ProductResponseDTO> productResponseDTOS = products.stream()
                .map(ProductServiceImpl::makeProductResponseDTO)
                .collect(Collectors.toList());
        dto.setProducts(productResponseDTOS);
        return dto;
    }
}
