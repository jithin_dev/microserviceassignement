package com.jithin.productService.service;

import com.jithin.productService.dto.request.OrderRequestDTO;
import com.jithin.productService.dto.response.OrderResponseDTO;

import java.util.List;

public interface OrderService {
    OrderResponseDTO createOrder(OrderRequestDTO dto);

    OrderResponseDTO updateOrder(int id, OrderRequestDTO dto);

    List<OrderResponseDTO> getOrders();

    OrderResponseDTO getOrder(int id);

    void deleteOrder(int id);
}
