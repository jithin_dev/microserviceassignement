package com.jithin.productService.service;

import com.jithin.productService.dto.request.ProductRequestDTO;
import com.jithin.productService.dto.response.ProductResponseDTO;

import java.util.List;

public interface ProductService {

    List<ProductResponseDTO> getAllProducts();

    ProductResponseDTO getProduct(int id);

    ProductResponseDTO submitProduct(ProductRequestDTO dto);

    ProductResponseDTO updateProduct(int id, ProductRequestDTO dto);

    void deleteProduct(int id);
}
